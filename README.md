# i3wm configurations

Using the tokyo night colors for the top bar, and window borders.

Depends on:
 - `maim` for screenshot commands (can be changed to scrot if prefered)
 - `dunst` for display notifications
 - `feh` for the desktop background
 - `pasystray` for the volume control icon
 - `pactl` for the media keys shortcuts
 - `xss-lock` and `nm-applet` as the defaults from i3 for locking the screen and network tray icon
 - JetBrains Mono font with icons
 - Alacritty as the selected terminal (same colors available)